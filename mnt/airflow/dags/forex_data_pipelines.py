import airflow
from airflow import DAG
from datetime import datetime, timedelta
from airflow.sensors.http_sensor import HttpSensor
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from airflow.operators.email_operator import EmailOperator
from airflow.operators.slack_operator import SlackAPIPostOperator
import json
import csv
import requests

# Default arguments that will be used in all tasks of the DAG
default_args = {
    "owner" : "Airflow",                                # must be executed by the unix user airflow
    "start_date" : airflow.utils.dates.days_ago(1),     # when your DAG should start to be scheduled
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
    "email": "dev.brianpratama@gmail.com",
    "retries": 1,                                       # a task should be restarted at most once after a failure
    "retry_delay": timedelta(minutes=5)                  # the scheduler should wait for 5 minutes before restarting that task
}

def download_rates():
    with open('/usr/local/airflow/dags/files/forex_currencies.csv') as forex_currencies:
        reader = csv.DictReader(forex_currencies, delimiter=';')
        for row in reader:
            base = row['base']
            with_pairs = row['with_pairs'].split(' ')
            indata = requests.get('http://api.exchangeratesapi.io/latest?access_key=7f7e6aaa68e7c966e26780fad1b6faf0&base=' + base).json()
            outdata = {'base': base, 'rates': {}, 'last_update': indata['date']}
            for pair in with_pairs:
                outdata['rates'][pair] = indata['rates'][pair]
            with open('/usr/local/airflow/dags/files/forex_rates.json', 'a') as outfile:
                json.dump(outdata, outfile)
                outfile.write('\n')

# Instatiate a DAG object. This is pretty much a resource manager.
with DAG(dag_id="forex_data_pipeline",
        schedule_interval="@daily",
        default_args=default_args,
        catchup=False) as dag:
    
    is_forex_rates_available = HttpSensor(
        task_id="is_forex_rates_available",
        method='GET',
        http_conn_id='forex_api',   # the name of the connection we will create in Airflow
        endpoint='latest?access_key=7f7e6aaa68e7c966e26780fad1b6faf0',
        response_check=lambda response: "rates" in response.text,   # lambda is a one line function that we don't need to name it.
                                                                    # Returns true if the string "rates" exists in the response given
                                                                    # by the sensor each time an HTTP request is sent.
        poke_interval=5,    # HTTPSensor should send an HTTP request every 5 seconds
        timeout=20          # during at most 20 seconds before timing out
    )

    is_forex_currencies_file_available = FileSensor(
        task_id="is_forex_currencies_file_available",
        fs_conn_id="forex_path",
        filepath="forex_currencies.csv",
        poke_interval=5,
        timeout=20
    )

    downloading_rates = PythonOperator(
        task_id="downloading_rates",
        python_callable=download_rates
    )

    saving_rates = BashOperator(
        task_id="saving_rates",
        bash_command="""
            hdfs dfs -mkdir -p /forex && \
                hdfs dfs -put -f $AIRFLOW_HOME/dags/files/forex_rates.json /forex
        """
    )

    creating_forex_rates_table = HiveOperator(
        task_id="creating_forex_rates_table",
        hive_cli_conn_id="hive_conn",
        hql="""
            CREATE EXTERNAL TABLE IF NOT EXISTS forex_rates(
                base STRING,
                last_update DATE,
                eur DOUBLE,
                usd DOUBLE,
                nzd DOUBLE,
                gbp DOUBLE,
                jpy DOUBLE,
                cad DOUBLE
                )
            ROW FORMAT DELIMITED
            FIELDS TERMINATED BY ','
            STORED AS TEXTFILE
        """
    )

    forex_processing = SparkSubmitOperator(
        task_id="forex_processing",
        conn_id="spark_conn",
        application="/usr/local/airflow/dags/scripts/forex_processing.py",
        verbose=False
    )

    sending_email_notification = EmailOperator(
        task_id="sending_email",
        to="ap.brian717@gmail.com",
        subject="forex_data_pipeline",
        html_content="<h3>forex_data_pipeline succeeded</h3>"
    )

    sending_slack_notification = SlackAPIPostOperator(
        task_id="sending_slack",
        token="xoxb-1922261161522-1934909719873-6HWFaJT0JNHiNMQ0iXGOLTLD",
        username="airflow",
        text="DAG forex_data_pipeline: DONE",
        channel="#airflow-exploit"
    )

    # define the dependencies between tasks
    is_forex_rates_available >> is_forex_currencies_file_available >> downloading_rates >> saving_rates
    saving_rates >> creating_forex_rates_table >> forex_processing
    forex_processing >> sending_email_notification >> sending_slack_notification
